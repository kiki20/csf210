const e = require("express")
const express = require("express")
const app = express()
// const userRouter = require('./routes/userRoutes')
const userRouter= require('./routes/userRoutes')

app.use(express.json())
app.use('/api/v1/users', userRouter)

// const port =4001
// app.listen(port, ()=>{
//     console.log(`App running on port ${port} ..`)
// })
module.exports= app